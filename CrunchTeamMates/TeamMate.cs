﻿using Microsoft.Office.Interop.Excel;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using excel = Microsoft.Office.Interop.Excel;


namespace CrunchTeamMates
{
    class TeamMate
    {
        public string Name { get; set; }
        public string Role { get; set; }

        public string picture { get; set; }

        public List<string> emails = new List<string>();
    }
}
