﻿
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using excel = Microsoft.Office.Interop.Excel;
using OpenQA.Selenium.Support.UI;
using Microsoft.Office.Interop.Excel;
using System.IO;
using System.Net;
using System.Threading;
using System.Diagnostics;

namespace CrunchTeamMates
{
    class TeamMatesList
    {
        IWebDriver driver;
        OpenFileDialog openFile;
        excel.Application excelApp;
        object misValue = System.Reflection.Missing.Value;
        List<string> rowsNames;
        excel.Workbooks excelWB;
        excel._Worksheet sheets;
        excel.Range last;
        excel.Range range;



        List<List<TeamMate>> personList;
        public void SslCertification(string url)
        {
            ServicePointManager.Expect100Continue = true;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            var request = (HttpWebRequest)WebRequest.Create(url);

            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0";

            request.Timeout = 10000;

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            //Console.WriteLine("Server status code: " + response.StatusCode);

            //using (StreamReader sr = new StreamReader(response.GetResponseStream()))
            //{
            //    string result = sr.ReadToEnd();

            //   listbox.Items.Add(result);
            //}
        }
        async Task KillExcellProcessAsync()
        {
            await Task.Run(() =>
            {
                foreach (Process clsProcess in Process.GetProcesses())
                {
                    if (clsProcess.ProcessName.Equals("EXCEL"))
                    {
                        clsProcess.Kill();
                        //break;
                    }
                }
            });
        }
        async Task ScrollToBottomAsync(IWebDriver driver, int numericUpDown)
        {
            IWebElement infoBase = driver.FindElement(By.XPath(".//*[@id='main-content']//div[@class = 'base info-tab people']"));
            List<IWebElement> teamMatesCount = infoBase.FindElements(By.TagName("li")).ToList();

            long scrollHeight = 0;

            if (numericUpDown > teamMatesCount.Count)
            {
                await Task.Run(() =>
                {
                    for (int i = 0; i < numericUpDown; i++)
                    {
                        List<IWebElement> peoples = infoBase.FindElements(By.TagName("li")).ToList();
                        if (peoples.Count >= numericUpDown)
                        {
                            break;
                        }
                        IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                        var newScrollHeight = (long)js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight); return document.body.scrollHeight;");

                        if (newScrollHeight == scrollHeight)
                        {
                            break;
                        }
                        else
                        {
                            scrollHeight = newScrollHeight;
                            Thread.Sleep(1500);
                        }
                    }
                });
            }
        }

        
        public async Task NavigateUrl(string combobox, System.Windows.Forms.ListBox listbox, int numericUpDown)
        {

            rowsNames = new List<string>();
            personList = new List<List<TeamMate>>();

            sheets = excelApp.ActiveSheet;
            //last = sheets.UsedRange.Rows;
            //range = sheets.get_Range("B1", last);

            //int lastUsedRow = last.Row;

            //int lastUsedRow = sheets.Range[2, sheets.Cells[1, sheets.Rows.Count]].End(excel.XlDirection.xlDown).Value;

            var lastUsedRow = excelApp.WorksheetFunction.CountA(sheets.Columns[combobox]);

            driver = new FirefoxDriver();
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));

            string match = null;

            await Task.Run(async () =>
            {
                

                for (int row = 2; row <= lastUsedRow; row++)
                {

                    try
                    {
                        match = sheets.Cells[row, combobox].value;

                        driver.Navigate().GoToUrl(match);
                        //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                        System.Threading.Thread.Sleep(10000);
                        //Trouble is here.
                        string urlsLast = "";
                        //string companyName = "";
                        var appendUrl = "";
                        List<IWebElement> list = driver.FindElements(By.XPath(".//dd[contains(., 'http://')]"))
                            .ToList(); //.//a[@target = '_blank' and @rel = 'nofollow']
                        if (list.Any())
                        {
                            urlsLast = list.First().Text;
                            appendUrl = urlsLast.Substring(urlsLast.Length - 4);
                        }
                        else
                        {
                            appendUrl = ".com";
                        } 
                    

                    string coName = driver.FindElement(By.Id("profile_header_heading")).Text;
                        

                        

                        if (match.Contains("/people"))
                        {
                            driver.Navigate().GoToUrl(match);
                        }
                        else if (match.Contains("#/entity"))
                        {
                            match.Replace("#/entity", "/people");
                            driver.Navigate().GoToUrl(match);
                        }
                        else
                        {
                            driver.Navigate().GoToUrl(match + "/people");
                        }



                        //var compUrl = companyName.FindElement(By.TagName("dd")).GetAttribute("href");
                        listbox.Invoke((MethodInvoker)delegate { listbox.Items.Add(coName); });
                        //listbox.Items.Add(coName);
                        IWebElement infoBase = driver.FindElement(By.XPath(".//*[@id='main-content']//div[@class = 'base info-tab people']"));

                        //Scroller
                        await ScrollToBottomAsync(driver, numericUpDown);


                        //IWebElement scroller = (IWebElement)((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0, document.body.scrollHeight - 150)");


                        List<IWebElement> teamMates = infoBase.FindElements(By.TagName("li")).ToList();



                        var companylist = new List<TeamMate>();
                        int peopleCounter = 1;
                        foreach (var item in teamMates)
                        {
                            if (peopleCounter <= numericUpDown)
                            {
                                var person = new TeamMate();
                                listbox.Invoke((MethodInvoker) delegate
                                {
                                    listbox.Items.Add(peopleCounter + "-" +
                                                      item.FindElement(By.TagName("a")).GetAttribute("title"));
                                });
                                person.Name = item.FindElement(By.TagName("a")).GetAttribute("title");
                                listbox.Invoke((MethodInvoker) delegate
                                {
                                    listbox.Items.Add(item.FindElement(By.TagName("h5")).Text);
                                });
                                person.Role = item.FindElement(By.TagName("h5")).Text;
                                listbox.Invoke((MethodInvoker) delegate
                                {
                                    listbox.Items.Add(item.FindElement(By.TagName("img")).GetAttribute("src"));
                                });
                                person.picture = item.FindElement(By.TagName("img")).GetAttribute("src");

                                string[] nameSp = person.Name.Split(new char[] { ' ' });

                                person.emails.AddRange(new string[]
                                {
                              nameSp[0] + nameSp[1] + "@" + coName + appendUrl,
                              nameSp[0] +"."+ nameSp[1] + "@" + coName + appendUrl,
                              nameSp[0] + "@" + coName + appendUrl,
                              nameSp[0] + nameSp[1][0] + "@" + coName + appendUrl,
                              nameSp[0] + "." + nameSp[1][0] + "@" + coName + appendUrl
                                });

                                foreach (var email in person.emails)
                                {
                                    listbox.Invoke((MethodInvoker) delegate { listbox.Items.Add(email); });
                                }


                                companylist.Add(person);
                                peopleCounter++;
                            }
                        }
                        personList.Add((companylist));
                    }
                    catch (Exception ex)
                    {
                        excelApp.Quit();
                        if (sheets != null)
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelApp);
                        // Empty variables
                        excelApp = null;
                        sheets = null;

                        // Force garbage collector cleaning

                        driver.Quit();
                        GC.Collect();
                        await KillExcellProcessAsync();
                        throw new Exception("System error." + ex.Message);
                    }
                }
            });  

            excelApp.Quit();
            if (sheets != null)
                System.Runtime.InteropServices.Marshal.ReleaseComObject(excelApp);
            // Empty variables
            excelApp = null;
            sheets = null;

            // Force garbage collector cleaning

            driver.Quit();
            await KillExcellProcessAsync();
            GC.Collect();
            MessageBox.Show("Parsing complete!");
        }

        public async Task ImportXLS(ComboBox comboBox)
        {
            //fix SSL certificate
            
            openFile = new OpenFileDialog();
            excelApp = new excel.Application();

            if (openFile.ShowDialog() == DialogResult.OK)
            {

                excelWB = excelApp.Workbooks;

                excelWB.Open(openFile.FileName);

                excel.Worksheet activeSheet = excelApp.ActiveSheet;


                for (int c = 1; c < 100; c++)
                {
                    if (activeSheet.Cells[1, c].value != null)
                    {
                        string columnName = activeSheet.Columns[c].Address;
                        Regex reg = new Regex(@"(\$)(\w*):");
                        if (reg.IsMatch(columnName))
                        {
                            Match match = reg.Match(columnName);
                            comboBox.Invoke((MethodInvoker)delegate { comboBox.Items.Add(match.Groups[2].Value); });
                            //comboBox.Items.Add(match.Groups[2].Value);
                        }
                    }
                }
            }
        }

        public async Task ExportXLS(ComboBox comboBox)
        {
            await KillExcellProcessAsync();
            excel.Application excelAppExp = new excel.Application();
            excel.Workbook workbook = excelAppExp.Workbooks.Open(openFile.FileName);
            excel.Worksheet workSheet = (Worksheet)workbook.Worksheets[1];
            workSheet.Activate();
            Match match = null;
            object misvalue = System.Reflection.Missing.Value;
            Range imgRange;
            float Left;
            float Top;
            int columnCounter = 1;

            try
            {
                for (int c = 1; c < 100; c++)
                {
                    if (workSheet.Cells[1, c].value != null)
                    {
                        string columnName = workSheet.Columns[c].Address;
                        Regex reg = new Regex(@"(\$)(\w*):");
                        if (reg.IsMatch(columnName))
                        {
                            match = reg.Match(columnName);
                            columnCounter++;
                        }
                    }
                }
                int col = columnCounter + 1;
                int row = 2;
                //int col = match.Groups[2].Index;

                for (; col < columnCounter + personList.Count; col++)
                {
                    foreach (var company in personList)
                    {
                        col = columnCounter + 1;
                        foreach (var person in company)
                        {
                            workSheet.Cells[1, col] = "Name";
                            workSheet.Cells[1, col + 1] = "PrimaryRole";
                            workSheet.Cells[1, col + 2] = "Picture url";
                            workSheet.Cells[1, col + 3] = "Emails";

                            workSheet.Cells[row, col++] = person.Name;
                            workSheet.Cells[row, col++] = person.Role;
                            workSheet.Cells[row, col++] = person.picture;
                            //imgRange = (Range) workSheet.Cells[row, col++];
                            //imgRange.EntireRow.RowHeight = 32;
                            //Left = (float) imgRange.Left;
                            //Top = (float) imgRange.Top;
                            //const float ImageSize = 32;
                            //try
                            //{
                            //    workSheet.Shapes.AddPicture(person.picture, Microsoft.Office.Core.MsoTriState.msoFalse,
                            //        Microsoft.Office.Core.MsoTriState.msoCTrue, Left, Top, ImageSize, ImageSize);
                            //}
                            //catch
                            //{

                            //}



                            foreach (var email in person.emails)
                            {
                                workSheet.Cells[row, col++] = email;
                            }
                        }
                        row++;
                    }
                }

                //workSheet.Range["A1"].AutoFormat(Microsoft.Office.Interop.Excel.XlRangeAutoFormat.xlRangeAutoFormatColor1);



                Range range = workSheet.get_Range("A1", misvalue);
                range.RowHeight = 14.00;


            }
            catch (Exception ex)
            {
                if (workSheet != null)
                    throw new Exception("There was a PROBLEM saving Excel file!\n" + ex.Message);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(excelAppExp);

                if (workbook != null)
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(excelAppExp);

                // Empty variables
                excelAppExp = null;
                workSheet = null;
                await KillExcellProcessAsync();
                // Force garbage collector cleaning
                GC.Collect();
            }
            finally
            {

                //string fileName = string.Format(@"D:\CrunchBookSaved.xlsx", Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory));

                //if (File.Exists(fileName))
                //{
                //    File.Delete(fileName);
                //}
                // Save this data as a file
                //workSheet.SaveAs(fileName);
                //excelExport.Quit();

                excelAppExp.ActiveWorkbook.Save();
                excelAppExp.ActiveWorkbook.Saved = true;

                if (excelAppExp.ActiveWorkbook.Saved == true)
                {
                    MessageBox.Show("Excel file is generated!");
                }

                // Release COM objects (very important!)
                if (workSheet != null)
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(excelAppExp);

                if (workbook != null)
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(excelAppExp);

                // Empty variables
                excelAppExp = null;
                workSheet = null;
                await KillExcellProcessAsync();
                // Force garbage collector cleaning
                GC.Collect();

            }
        }
    }
}

