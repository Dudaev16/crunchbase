﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrunchTeamMates
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
            
    }
        TeamMatesList teamcontrols = new TeamMatesList();
        private async void btnExcelUpload_ClickAsync(object sender, EventArgs e)
        {
            await teamcontrols.ImportXLS(comboBox);
        }

        private async void btnSearch_ClickAsync(object sender, EventArgs e)
        {
            await teamcontrols.NavigateUrl(comboBox.Text, listBox1, (int) numericUpDown1.Value);
        }

        private async void btnExport_ClickAsync(object sender, EventArgs e)
        {
            await teamcontrols.ExportXLS(comboBox);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
           
        }
    }
}
